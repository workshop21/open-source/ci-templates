# CI templates 

Customized stage templates for CI/CD solutions running on gitlab 11.4.

# example

```yaml
include:
  - 'https://gitlab.com/workshop21/open-source/ci-templates/raw/master/bake_release.yaml'
  - 'https://gitlab.com/workshop21/open-source/ci-templates/raw/master/build.yaml'
  - 'https://gitlab.com/workshop21/open-source/ci-templates/raw/master/codequality.yaml'
  - 'https://gitlab.com/workshop21/open-source/ci-templates/raw/master/tag.yaml'
  - 'https://gitlab.com/workshop21/open-source/ci-templates/raw/master/deploySta.yaml'
  - 'https://gitlab.com/workshop21/open-source/ci-templates/raw/master/deployPre.yaml'
  - 'https://gitlab.com/workshop21/open-source/ci-templates/raw/master/deploySingleProd.yaml'

stages:
  - tag
  - build
  - bake_release
  - deploy-staging
  - deploy-pre-production
  - deploy-production

variables:
  language: go
  TEMPLATES: "deployment,ingress,service"
  GODEP: "https://git.some-domain.ch/ewa/auth/service@master,https://git.some-domain.ch/src/pkg@master,https://git.some-domain.ch/src/pkg/some-domain@master,https://git.some-domain.ch/ewa/auth/protoc-gen-vrsgauthinterceptor@master,https://git.some-domain.ch/ewa/someservice/service@master,https://git.some-domain.ch/ewa/anotherservice/service@master,https://git.some-domain.ch/ewa/randomservice/service@master,https://github.com/satori/go.uuid@v1.1.0"
  AEROSPIKE: "NO"
  INGRESS_PATH: "/"
  HEALTHZ: "NOU!"
  INGRESS_CLASS: "conduit"
  IP_WHITELIST: "<a list of ip's or ranges, e.g.: 34.45.65.12/22,87.37.23.2/32>"
  
  #STA
  STA_NAMESPACE: "sta-${CI_PROJECT_NAMESPACE}"
  STA_BASE_URL: "sta.some-subdomain.some-domain.ch"
  STA_INGRESS_CLASS: "conduit"
  STA_STORAGE_ENV: "STA"
  STA_IP_WHITELIST: "<a list of ip's or ranges, e.g.: 34.45.65.12/22,87.37.23.2/32>"
  
  #PRE
  PRE_NAMESPACE: "pre-${CI_PROJECT_NAMESPACE}"
  PRE_BASE_URL: "pre.some-subdomain.some-domain.ch"
  PRE_INGRESS_CLASS: "conduit"
  PRE_STORAGE_ENV:  "PRE"
  PRE_IP_WHITELIST: "<a list of ip's or ranges, e.g.: 34.45.65.12/22,87.37.23.2/32>"
  
  #SINGLE_PROD
  PROD_NAMESPACE: "prod-${CI_PROJECT_NAMESPACE}"
  PROD_BASE_URL: "some-subdomain.some-domain.ch"
  PROD_INGRESS_CLASS: "conduit"
  PROD_STORAGE_ENV: "PROD"
  PROD_IP_WHITELIST: "<a list of ip's or ranges, e.g.: 34.45.65.12/22,87.37.23.2/32>"
  
  #MULTI_PROD
  STA_PROD_NAMESPACE: "prod-${CI_PROJECT_NAMESPACE}"
  STA_PROD_BASE_URL: "int.some-subdomain.some-domain.ch"
  STA_PROD_INGRESS_CLASS: "conduit"
  STA_PROD_STORAGE_ENV: "STA"
  STA_PROD_IP_WHITELIST: "<a list of ip's or ranges, e.g.: 34.45.65.12/22,87.37.23.2/32>"
  
  PRE_PROD_NAMESPACE: "prod-${CI_PROJECT_NAMESPACE}"
  PRE_PROD_BASE_URL: "test.some-subdomain.some-domain.ch"
  PRE_PROD_INGRESS_CLASS: "conduit"
  PRE_PROD_STORAGE_ENV: "PRE"
  PRE_PROD_IP_WHITELIST: "<a list of ip's or ranges, e.g.: 34.45.65.12/22,87.37.23.2/32>"
  
  PROD_PROD_NAMESPACE: "prod-${CI_PROJECT_NAMESPACE}"
  PROD_PROD_INGRESS_CLASS: "conduit"
  PROD_PROD_STORAGE_ENV: "PROD"
  PROD_PROD_BASE_URL: "some-subdomain.some-domain.ch"
  PROD_PROD_IP_WHITELIST: "<a list of ip's or ranges, e.g.: 34.45.65.12/22,87.37.23.2/32>"







```